from tkinter import *

#se escribe en un archivo la informacion de cada carga
def escribirArchivo(event):
	datos_carga = data_entry.get()
	archivoInformacion = open("cargas.txt", "a")
	archivoInformacion.write(nombre)
	archivoInformacion.write(" ")
	archivoInformacion.write(datos_carga)
	archivoInformacion.write("\n")
	archivoInformacion.close()
	agregado = Label(window, text="Agregado", fg ='yellow', bg = 'green', relief = 'solid', font=("arial",12,"bold"))
	agregado.place(x=posXX+320, y=posYY-0.5) #relief = GROOVE, RIDGE, SUNKEN, RAISED


#botones para checkear las cargas
def botones(nombreCarga, posX, posY):
	checkCarga = Checkbutton(window, text=nombreCarga, command=lambda: cargas(nombreCarga, posX, posY))
	checkCarga.place(x=posX, y=posY)


def cargas(nombreCarga, posX, posY):
	#se crean variables	globales para que puedan ser usadas fuera de la funcion
	global data_entry, nombre, posXX, posYY
	posXX = posX
	posYY = posY
	data_entry = Entry(window) #informacion que ingresa el usuario
	nombre = nombreCarga
	#cada vez que se presiona "enter" se llama a la funcion escribirArchivo
	data_entry.bind("<Return>", escribirArchivo)
	data_entry.place(x=posX+140, y=posY)

listaDeCargas = ['Cocina', 'Refrigerador', 'Microondas', 'CoffeMaker', 'Licuadora', 'Freidora', 'Arrocera',
                'Aire_Acondicionado', 'Iluminación', 'Lavadora', 'Secadora', 'Plancha', 'Televisores','Celulares',
				'Computadoras','Duchas']

archivoInformacion = open("cargas.txt", "w")

####################################SE CREA Y DISENA LA VENTANA################################
window = Tk()
window.geometry("600x575")
window.title("Diseno electrico residencial")
frame = Frame(window).place(relwidth=1, relheight=1)
####################################SE CREA Y DISENA LA VENTANA#################################

####################TEXTOS A IMPRIMIR####################################
titulo = Label(window, text="Ingreso de cargas residenciales", fg ='yellow', bg = 'blue', relief = 'solid', font=("arial",16,"bold"))
titulo.place(x=135, y=10)

textoCarga = Label(window, text="Por favor ingrese las cargas residenciales", fg ='black', font=("arial",13,"bold"))
textoCarga.place(x=30, y=55)

textoPotencia = Label(window, text="Utilice el siguiente formato: <Potencia>  <Factor de potencia>  <Voltaje Nominal>", fg ='black', font=("arial",10,"normal"))
textoPotencia.place(x=30, y=80)
####################TEXTOS A IMPRIMIR####################################

#posiciones iniciales:
x=30
y=105

for i in range(0, len(listaDeCargas)):
	nombreCarga = listaDeCargas[i]
	botones(nombreCarga, x, y)
	y += 25

##############################BOTON CERRAR#################################
cerrar = Button(window, text="Cerrar", fg ='yellow', bg = 'red', relief = RAISED, font=("arial",12,"bold"), command=exit)
cerrar.place(x=247, y=525) #relief = GROOVE, RIDGE, SUNKEN, RAISED
##############################BOTON CERRAR#################################

window.mainloop()
