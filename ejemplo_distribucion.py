#!/usr/bin/python3
import math   # This will import math module
from tkinter import *
from math import tan, acos, radians, sqrt, pi
from cmath import rect, polar

#Se define una clase llamada Carga, con los atributos caracteristicos de una carga: name, potencia, y factor de potencia.
class Carga:
    def __init__(self,name,power_value, power_factor, nominal_voltage):
        self.name = name
        self.power_value = power_value
        self.power_factor = power_factor
        self.nominal_voltage = nominal_voltage

#La siguiente funcion almacena los datos obtenidos del usuario en la ventana interactiva a una lista de cargas.
def lectura_cargas(archivo_lectura):
    archivo = open(archivo_lectura, "r")

    cargas_120v = list()
    cargas_240v = list()

    for line in archivo:
        file_line = line.split(" ")
        name = file_line[0]
        power_value = file_line[1]
        power_factor = file_line[2]
        nominal_voltage = file_line[3]
        #El siguiente condicional analiza si la carga esta conectada a 120 V o a 240 V, y las reparte en la lista correspondiente.
        if (nominal_voltage.rstrip() == "120"):
            cargas_120v.append(Carga(name, float(power_value), float(power_factor),int(nominal_voltage.rstrip())))
        else:
            cargas_240v.append(Carga(name, float(power_value), float(power_factor),int(nominal_voltage.rstrip())))

# La siguiente linea ordena la lista de cargas a 120V y 240V respectivamente por valor de potencia (mayor a menor).
# Se utiliza la funcion sort, la cual forma parte de las funciones predeterminadas de python.
# lambda retorna el valor de la potencia de cada carga, y sort lo ordena de mayor a menor.
    cargas_120v.sort(key = lambda x: x.power_value,reverse = True)
    return cargas_120v,cargas_240v

#La siguiente funcion distribuye e imprime las cargas. Recibe dos listas, una con las cargas a 120 V, y otra con las que se conectana 240 V.
#Para realizar la distribucion, asigna a la linea A el mayor valor de potencia consumida por una carga, y a la linea B la segunda carga con mayor consumo.
#Posteriormente compara ambas lineas, si la linea A es mayor a la linea B, la funcion asigna una carga mas a la linea B (esto se repite hasta que ambas cargas
#sean iguales o se terminen todas las cargas ingresadas por el usuario). Por el contrario, si la linea B pasa a ser mayor a la linea A, la siguiente carga se
#almacena en la linea A (asi sucesivamete hasta que ambas cargas sean iguales o se terminen todas las cargas ingresadas por el usuario).

def line_distribution(lista_cargas_120v,lista_cargas_240v):
	linea_A = [] #Lista que almacena los elementos entre la linea A y el neutro.
	linea_B = [] #Lista que almacena los elementos entre la linea B y el neutro.

	QTotalA = 0
	QTotalB = 0
	QTotalAB = 0

	for i in range(len(lista_cargas_120v)-1):

		if (i==0):
			linea_A.append(lista_cargas_120v[0])
			linea_B.append(lista_cargas_120v[1])
			power_A = lista_cargas_120v[0].power_value
			power_B = lista_cargas_120v[1].power_value
			#potencias reactivas:
			QTotalA += power_A * tan(acos(lista_cargas_120v[0].power_factor))
			QTotalB += power_B * tan(acos(lista_cargas_120v[1].power_factor))
		elif (power_A > power_B):
			linea_B.append(lista_cargas_120v[i+1])
			power_B = power_B + lista_cargas_120v[i+1].power_value
			#potencia reactiva:
			QTotalB += power_B * tan(acos(lista_cargas_120v[i+1].power_factor))
		else:
			linea_A.append(lista_cargas_120v[i+1])
			power_A = power_A + lista_cargas_120v[i+1].power_value
			#potencia reactiva:
			QTotalA += power_A * tan(acos(lista_cargas_120v[i+1].power_factor))

	power_AB = 0

	for i in range(len(lista_cargas_240v)):
	    power_AB += lista_cargas_240v[i].power_value
	    #potencia reactiva:
	    QTotalAB += power_AB * tan(acos(lista_cargas_240v[i].power_factor))

	return power_A,power_B,linea_A,linea_B,power_AB, QTotalA, QTotalB, QTotalAB

#################INICIO FUNCION CALCULO DEL CALIBRE DEL NEUTRO##################

#Para el calculo del calibre del neutro se establece la siguiente funcion.
#Esta considera que todas las cargas tienen factor de potencia 1, por lo cual la
#potencia aparente y activa es la misma.
def calibre_neutro (lista_cargas_120v,lista_cargas_240v):
    Power_S = 0
    voltaje_entre_fases = 240
    Corriente_N = 0
    factor_demanda_240 = 0.7
    factor_demanda_carga_individual = 0.75
    temperature_line = 0

    if(len(lista_cargas_240v) != 0):
        for i in range(len(lista_cargas_240v)):
            Power_S += lista_cargas_240v[i].power_value*factor_demanda_240 #Las cargas a 240 se consideran al 70%

    if(len(lista_cargas_120v) != 0):
        for i in range(len(lista_cargas_120v)):
            #Se toman en cuentan iluminacion + electrodomesticos pequeños + lavanderia al 100%
            if(lista_cargas_120v[i].name == ('CoffeMaker' or 'Licuadora' or 'Arrozera' or 'Freidora'
            or 'Iluminación' or 'Televisores' or 'Computadoras' or 'Lavadora')):
                Power_S += lista_cargas_120v[i].power_value
            else:
                Power_S += lista_cargas_120v[i].power_value*factor_demanda_carga_individual #Cargas individuales no fijas se consideran al 75%

    Corriente_N = (Power_S / voltaje_entre_fases)

    if(Corriente_N <= 100):
        archivo_calibre = open("calibre_conductores_60C.csv","r");
    else:
        archivo_calibre = open("calibre_conductores_75C.csv","r");

    temperature_line = archivo_calibre.readline()

    for line in archivo_calibre:
        archivo_calibre_line = line.split(",")
        calibre = archivo_calibre_line[0]
        ampacidad = archivo_calibre_line[1]
        if(int(ampacidad.rstrip()) >= Corriente_N):
            return calibre,Corriente_N,temperature_line[0:2]
            break

    archivo_calibre.close()
####################FIN FUNCION CALCULO DEL CALIBRE DEL NEUTRO##################

#####################CALCULOS PARA CORRIENTE####################################
def calibre_fase(power_A,power_B,power_AB):
    #Para la corriente por AB:
    SAB = sqrt(power_AB**2 + QTotalAB**2)  #potencia aparente
    corrienteAB = list() #lista para guardar la magnitud y el angulo de la corriente
    corrienteAB.append(SAB/240) #porque S=|V|*|I|
    fpTotalAB = power_AB/SAB  #factor de potencia
    thetaAB = acos(fpTotalAB)*180/pi #angulo
    #La tension es 240 angulo de 0, entonces theta = alfa - phi = 0 - phi
    #entonces phi = -theta
    corrienteAB.append(-thetaAB)
    corrienteABCartesiana = rect(corrienteAB[0], corrienteAB[1]) #conversion a forma cartesiana

    #corriente por la fase A:
    SA = sqrt(power_A**2 + QTotalA**2)  #potencia aparente
    corrienteA = list() #lista para guardar la magnitud y el angulo de la corriente
    corrienteA.append(SA/120) #porque S=|V|*|I|
    fpTotalA = power_A/SA  #factor de potencia
    thetaA = acos(fpTotalA)*180/pi #angulo
    #La tension es 120 angulo de 0, entonces theta = alfa - phi = 0 - phi
    #entonces phi = -theta
    corrienteA.append(-thetaA)
    corrienteACartesiana = rect(corrienteA[0], corrienteA[1])

    #Para la corriente por B:
    SB = sqrt(power_B**2 + QTotalB**2)  #potencia aparente
    corrienteB = list() #lista para guardar la magnitud y el angulo de la corriente
    corrienteB.append(SB/120) #porque S=|V|*|I|
    fpTotalB = power_B/SB  #factor de potencia
    thetaB = acos(fpTotalB)*180/pi #angulo
    #La tension es 120 angulo de 0, entonces theta = alfa - phi = 0 - phi
    #entonces phi = -theta
    corrienteB.append(-thetaB)
    corrienteBCartesiana = rect(corrienteB[0], corrienteB[1])

    #corrientes "finales"
    corrienteaA = corrienteACartesiana + corrienteABCartesiana
    corrienteBb = corrienteBCartesiana + corrienteABCartesiana
    magnitudCorrienteaA = polar(corrienteaA)[0]
    magnitudCorrienteBb = polar(corrienteBb)[0]

    #Se considera unicamente la corriente de mayor magnitud en las fases para
    #realizar el calculo del calibre
    if(magnitudCorrienteaA > magnitudCorrienteBb):
        corrienteMayor = magnitudCorrienteaA
    else:
        corrienteMayor = magnitudCorrienteBb

    if(corrienteMayor <= 100):
        archivo_calibre = open("calibre_conductores_60C.csv","r");
    else:
        archivo_calibre = open("calibre_conductores_75C.csv","r");

    temperature_line = archivo_calibre.readline()

    for line in archivo_calibre:
        archivo_calibre_line = line.split(",")
        calibre = archivo_calibre_line[0]
        ampacidad = archivo_calibre_line[1]
        if(int(ampacidad.rstrip()) >= corrienteMayor):
            return calibre,corrienteMayor,temperature_line[0:2]
            break

    archivo_calibre.close()
#####################FIN CALCULOS PARA CORRIENTE################################

def breakers_circuitos_ramales(lista_cargas_120v,lista_cargas_240v):
    carga_maxima_breaker = 0.8 #Un dispositivo de protección breaker o fusible no deberá ser cargado más de un 80% de su capacidad nominal.
    numero_circuitos_electrodomesticos = 0
    numero_circuitos_iluminacion = 0
    Power_electrodomesticos = 0
    Corriente_lavadora = 0
    breaker_lavadora = ""
    Power_iluminacion = 0
    breaker_iluminacion = 0
    Corriente_cocina = 0
    breaker_cocina = 0
    Corriente_aire = 0
    breaker_aire = 0
    Corriente_secadora = 0
    breaker_secadora = 0
    Corriente_ducha = 0
    breaker_ducha = 0

    if(len(lista_cargas_120v) != 0):
        for i in range(len(lista_cargas_120v)):
            #Circuitos y breakers para electrodomesticos pequeños o que generalmente no consumen mas de 2000 W.
            #Al incluir la cocina, se piensa en una plantilla a 120 V pequeña
            #NEC recomienda disyuntores de 15 o 20 A para este tipo de cargas
            if lista_cargas_120v[i].name in ['CoffeMaker','Licuadora','Arrozera','Freidora','Televisores','Computadoras','Refrigerador','Cocina','Plancha','Microondas']:
                Power_electrodomesticos = Power_electrodomesticos + lista_cargas_120v[i].power_value
            elif(lista_cargas_120v[i].name == ('Lavadora')):
                Corriente_lavadora+= (lista_cargas_120v[i].power_value)/(120)
            elif(lista_cargas_120v[i].name == ('Duchas')):
                Corriente_ducha+= (lista_cargas_120v[i].power_value)/(120)
            elif(lista_cargas_120v[i].name == ('Iluminación')):
                Power_iluminacion+= (lista_cargas_120v[i].power_value)

    if(len(lista_cargas_240v) != 0):
        for i in range(len(lista_cargas_240v)):
            if(lista_cargas_240v[i].name == ('Duchas')):
                Corriente_ducha += (lista_cargas_240v[i].power_value)/(240)
            elif(lista_cargas_240v[i].name == ('Cocina')):
                Corriente_cocina += (lista_cargas_240v[i].power_value)/(240)
            elif(lista_cargas_240v[i].name == ('Aire_Acondicionado')):
                Corriente_aire += (lista_cargas_240v[i].power_value)/(240)
            elif(lista_cargas_240v[i].name == ('Secadora')):
                Corriente_secadora += (lista_cargas_240v[i].power_value)/(240)

    #Para breaker de 20 A
    if(Power_electrodomesticos != 0):
        numero_circuitos_electrodomesticos  = math.ceil((Power_electrodomesticos)/(20*120*carga_maxima_breaker))

    #Para breaker de 20 A
    if(Power_iluminacion != 0):
        numero_circuitos_iluminacion  = math.ceil((Power_iluminacion)/(20*120*carga_maxima_breaker))

    archivo_breaker = open("breakers.txt","r")

    if(Corriente_lavadora != 0):
        for line in archivo_breaker:
            archivo_calibre_line = line
            ampacidad = archivo_calibre_line
            if(int(ampacidad.rstrip()) >= Corriente_lavadora):
                breaker_lavadora = int(ampacidad.rstrip())
                break
    archivo_breaker.close()
    archivo_breaker = open("breakers.txt","r")
    if(Corriente_ducha != 0):
        for line in archivo_breaker:
            archivo_calibre_line = line
            ampacidad = archivo_calibre_line
            if(int(ampacidad.rstrip()) >= Corriente_ducha):
                breaker_ducha = int(ampacidad.rstrip())
                break

    archivo_breaker.close()
    archivo_breaker = open("breakers.txt","r")

    if(Corriente_aire != 0):
        for line in archivo_breaker:
            archivo_calibre_line = line
            ampacidad = archivo_calibre_line
            if(int(ampacidad.rstrip()) >= Corriente_aire):
                breaker_aire = int(ampacidad.rstrip())
                break

    archivo_breaker.close()
    archivo_breaker = open("breakers.txt","r")

    if(Corriente_cocina != 0):
        for line in archivo_breaker:
            archivo_calibre_line = line
            ampacidad = archivo_calibre_line
            if(int(ampacidad.rstrip()) >= Corriente_cocina):
                breaker_cocina = int(ampacidad.rstrip())
                break

    archivo_breaker.close()
    archivo_breaker = open("breakers.txt","r")

    if(Corriente_secadora != 0):
        for line in archivo_breaker:
            archivo_calibre_line = line
            ampacidad = archivo_calibre_line
            if(int(ampacidad.rstrip()) >= Corriente_secadora):
                breaker_secadora = int(ampacidad.rstrip())
                break

    archivo_breaker.close()

    return numero_circuitos_electrodomesticos,numero_circuitos_iluminacion,breaker_lavadora,breaker_ducha,breaker_aire,breaker_cocina,breaker_secadora
######################VENTANA 2 PARA DESPLEGAR RESULTADOS#######################
def segunda_ventana(ventanaResultante):
    ventanaResultante.destroy()

    ventanaResultante2 = Tk()
    ventanaResultante2.geometry("780x250")
    ventanaResultante2.title("Diseno electrico residencial")

    titulo = Label(ventanaResultante2, text="Breakers a utilizar", fg ='yellow', bg = 'blue', relief = 'solid', font=("arial",16,"bold"))
    titulo.place(x=300, y=10)

    posx=30
    posy=50
    cambioEnY = 0
    interlineado = 25
    numero_circuitos_electrodomesticos = 0
    numero_circuitos_iluminacion = 0
    breaker_lavadora = 0
    breaker_ducha = 0
    breaker_aire = 0
    breaker_cocina = 0
    breaker_secadora = 0
    numero_circuitos_electrodomesticos,numero_circuitos_iluminacion,breaker_lavadora,breaker_ducha,breaker_aire,breaker_cocina,breaker_secadora = breakers_circuitos_ramales(lista_cargas_120v,lista_cargas_240v)

    textobreaker = "Los breaker a utilizar son:"

    label_textobreaker = Label(ventanaResultante2, text=textobreaker, fg ='black', font=("arial",13,"bold"))
    label_textobreaker.place(x=30, y=posy+cambioEnY)
    cambioEnY += interlineado

    textoelectrodomesticos = ""
    textoelectrodomesticos += "Breaker de 20 A para cada circuito. Se deben hacer: " + str(numero_circuitos_electrodomesticos) + " circuitos para electrodomesticos pequeños\n"

    label_textoelectrodomesticos = Label(ventanaResultante2, text = textoelectrodomesticos, fg = 'black', font = ("arial",13,"normal"), anchor = 'w')
    label_textoelectrodomesticos.place(x = 30, y = posy+cambioEnY)
    cambioEnY += interlineado

    textoiluminacion = ""
    textoiluminacion += "Breaker de 20 A para cada circuito. Se deben hacer: " + str(numero_circuitos_iluminacion) + " circuitos para iluminacion\n"

    label_textoiluminacion = Label(ventanaResultante2, text = textoiluminacion, fg = 'black', font = ("arial",13,"normal"), anchor = 'w')
    label_textoiluminacion.place(x = 30, y = posy+cambioEnY)
    cambioEnY += interlineado

    if(breaker_lavadora != 0):
        textolavadora = ""
        textolavadora += "El breaker de la lavadora debe ser de " + str(breaker_lavadora) + " A\n"
        label_textolavadora = Label(ventanaResultante2, text = textolavadora, fg = 'black', font = ("arial",13,"normal"), anchor = 'w')
        label_textolavadora.place(x = 30, y = posy+cambioEnY)
        cambioEnY += interlineado

    if(breaker_aire != 0):
        textoaire = ""
        textoaire += "El breaker del aire acondicionado debe ser de " + str(breaker_aire) + " A\n"
        label_textoaire = Label(ventanaResultante2, text = textoaire, fg = 'black', font = ("arial",13,"normal"), anchor = 'w')
        label_textoaire.place(x = 30, y = posy+cambioEnY)
        cambioEnY += interlineado

    if(breaker_ducha != 0):
        textoducha = ""
        textoducha += "El breaker de la ducha debe ser de " + str(breaker_ducha) + " A\n"
        label_textoducha = Label(ventanaResultante2, text = textoducha, fg = 'black', font = ("arial",13,"normal"), anchor = 'w')
        label_textoducha.place(x = 30, y = posy+cambioEnY)
        cambioEnY += interlineado

    if(breaker_cocina != 0):
        textococina = ""
        textococina += "El breaker de la cocina debe ser de " + str(breaker_cocina) + " A\n"
        label_textococina = Label(ventanaResultante2, text = textococina, fg = 'black', font = ("arial",13,"normal"), anchor = 'w')
        label_textococina.place(x = 30, y = posy+cambioEnY)
        cambioEnY += interlineado

    if(breaker_secadora != 0):
        textosecadora = ""
        textosecadora += "El breaker de la secadora debe ser de " + str(breaker_secadora) + " A\n"
        label_textosecadora = Label(ventanaResultante2, text = textosecadora, fg = 'black', font = ("arial",13,"normal"), anchor = 'w')
        label_textosecadora.place(x = 30, y = posy+cambioEnY)
        cambioEnY += interlineado

    ventanaResultante2.mainloop()
###################VENTANA 2 PARA DESPLEGAR RESULTADOS##########################

#Se asignan cargas a las lista correspondientes y se realiza la distribucion
lista_cargas_120v,lista_cargas_240v = lectura_cargas("cargas.txt")
power_A,power_B,linea_A,linea_B,power_AB, QTotalA, QTotalB, QTotalAB = line_distribution(lista_cargas_120v,lista_cargas_240v)
calibre_N, corriente_N, temperatura_neutro = calibre_neutro(lista_cargas_120v,lista_cargas_240v)
calibre_fase, corriente_fase, temperatura_fase = calibre_fase(power_A,power_B,power_AB)

#################VENTANA 1 PARA DESPLEGAR RESULTADOS#######################
ventanaResultante = Tk()
ventanaResultante.geometry("600x700")
ventanaResultante.title("Diseno electrico residencial")

titulo = Label(ventanaResultante, text="Resultado", fg ='yellow', bg = 'blue', relief = 'solid', font=("arial",16,"bold"))
titulo.place(x=240, y=10)

posx=30
posy=50
cambioEnY = 0
interlineado = 25

textoCargaA = "La carga en la fase A es de " + str(power_A) + " W"

cargaFaseA = Label(ventanaResultante, text=textoCargaA, fg ='black', font=("arial",13,"bold"))
cargaFaseA.place(x=30, y=posy+cambioEnY)
cambioEnY += interlineado

textoCargasA = ""

for x in range(len(linea_A)):
    textoCargasA += linea_A[x].name + " " + str(linea_A[x].power_value) + " W\n"
    cargasFaseA = Label(ventanaResultante, text=textoCargasA, fg ='black', font=("arial",12,))
    cargasFaseA.place(x=30, y=posy+cambioEnY)
    cambioEnY += interlineado
    textoCargasA = ""

textoCargaB = "La carga en la fase B es de " + str(power_B) + " W"

cargaFaseB = Label(ventanaResultante, text=textoCargaB, fg ='black', font=("arial",13,"bold"))
cargaFaseB.place(x=30, y=posy+cambioEnY)
cambioEnY += interlineado

textoCargasB = ""

for x in range(len(linea_B)):
    textoCargasB += linea_B[x].name + " " + str(linea_B[x].power_value) + " W\n"
    cargasFaseB = Label(ventanaResultante, text=textoCargasB, fg ='black', font=("arial",12,))
    cargasFaseB.place(x=30, y=posy+cambioEnY)
    cambioEnY += interlineado
    textoCargasB = ""


textoCarga240v = "La carga a 240 V es de " + str(power_AB) + " W"

cargaAB = Label(ventanaResultante, text=textoCarga240v, fg ='black', font=("arial",13,"bold"))
cargaAB.place(x=30, y=posy+cambioEnY)
cambioEnY += interlineado

textoCargas240v = ""
for i in range(len(lista_cargas_240v)):
    textoCargas240v += lista_cargas_240v[i].name + " " + str(lista_cargas_240v[i].power_value) + " W\n"
    cargasAB = Label(ventanaResultante, text=textoCargas240v, fg ='black', font=("arial",12), anchor='w')
    cargasAB.place(x=30, y=posy+cambioEnY)
    textoCargas240v = ""
    cambioEnY += interlineado

textocalibre = "Las corrientes nominales y los calibres a utilizar son:"

label_textocalibre = Label(ventanaResultante, text=textocalibre, fg ='black', font=("arial",13,"bold"))
label_textocalibre.place(x=30, y=posy+cambioEnY)
cambioEnY += interlineado

textoneutro = ""
textoneutro += "Corriente nominal neutro: " + str(round(corriente_N,3)) + " A\n"

label_textoneutro = Label(ventanaResultante, text = textoneutro, fg = 'black', font = ("arial",13,"normal"), anchor = 'w')
label_textoneutro.place(x = 30, y = posy+cambioEnY)
cambioEnY += interlineado

textoneutro = ""
textoneutro += "Calibre a utilizar: " + calibre_N + " AWG " + "a temperatura nominal de trabajo de " + temperatura_neutro + " °C"

label_textoneutro = Label(ventanaResultante, text = textoneutro, fg = 'black', font = ("arial",13,"normal"), anchor = 'w')
label_textoneutro.place(x = 30, y = posy+cambioEnY)
cambioEnY += interlineado

textofase = ""
textofase += "Corriente nominal fase: " + str(round(corriente_fase,3)) + " A\n"

label_textofase = Label(ventanaResultante, text = textofase, fg = 'black', font = ("arial",13,"normal"), anchor = 'w')
label_textofase.place(x = 30, y = posy+cambioEnY)
cambioEnY += interlineado

textofase = ""
textofase += "Calibre a utilizar: " + calibre_fase + " AWG " + "a temperatura nominal de trabajo de " + temperatura_fase + " °C"

label_textofase = Label(ventanaResultante, text = textofase, fg = 'black', font = ("arial",13,"normal"), anchor = 'w')
label_textofase.place(x = 30, y = posy+cambioEnY)
cambioEnY += interlineado


cerrar = Button(ventanaResultante, text="Continuar", fg ='yellow', bg = 'red', relief = RAISED,
         font=("arial",12,"bold"), command = lambda: (segunda_ventana(ventanaResultante)))
cerrar.place(x=247, y=665) #relief = GROOVE, RIDGE, SUNKEN, RAISED

ventanaResultante.mainloop()
